from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument
from mongoengine.fields import IntField

connection = connect(db="final_project", host="localhost", port=27017)
if connection:
    print("connection success")

class User(Document):
    email = StringField(unique= True)
    password = StringField()
    nama_lengkap = StringField()
    gender = StringField()
    no_telp = StringField()

class CRUD_user():
    def showUsers(self):
        try:
            result = []
            for doc in User.objects:
                user = {
                    "email" : doc.email,
                    "nama_lengkap" : doc.nama_lengkap,
                    "gender" : doc.gender,
                    "no_telp" : doc.no_telp
                }
                result.append(user)
            return result
        except Exception as e:
            print(e)
    def insertUser(self, **param):
        try:
            User(**param).save()
        except Exception as e:
            print(e)
    def deleteUser(self, param):
        try:
            doc = User.objects(email = param).first()
            doc.delete()
        except Exception as e:
            print(e)
    def updateUser(self, param):
        try:
            doc = User.objects(email = param['email']).first()
            doc.password = param['password']
            doc.nama_lengkap = param['nama_lengkap']
            doc.gender = param['gender']
            doc.no_telp = param['no_telp']
            doc.save()
        except Exception as e:
            print(e)
    def userbyemail(self, param):
        try:
            items = User.objects(email = param).first()
            user = {
                "email" : items.email,
                "password" : items.password,
                "nama_lengkap" : items.nama_lengkap,
                "gender" : items.gender,
                "no_telp" : items.no_telp
            }
            return user
        except Exception as e:
            print(e)
