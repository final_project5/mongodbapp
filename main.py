from fastapi import FastAPI
from user_route import router as user_router
from produk_route import router as produk_router

app = FastAPI()

app.include_router(user_router)
app.include_router(produk_router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}