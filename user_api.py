from pymongo import MongoClient
from models.user_models import CRUD_user as db
import csv, json
import json
from bson import ObjectId

db = db()

def ObjIdToStr(obj):
    return str(obj['_id'])

def insert_user(**param):
    db.insertUser(**param)
    data = {
        "message": "User Berhasil ditambahkan"
    }
    return data

def show_all():
    result = db.showUsers()
    return result

def delete_user_by_email(param):
    db.deleteUser(param)
    data = {
        "message": "data berhasil di hapus"
    }
    return data

def update_user_by_email(param):
    db.updateUser(param)
    data = {
        "message": "data berhasil di update"
    }
    return data

def show_user_by_email(param):
    result = db.userbyemail(param)
    return result