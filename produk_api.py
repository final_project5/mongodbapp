from pymongo import MongoClient
from models.produk_models import CRUD_produk as dbproduk
import csv, json
import json
from bson import ObjectId

db = dbproduk()

def ObjIdToStr(obj):
    return str(obj['_id'])

def insert_produk(**param):
    db.addproduk(**param)
    data = {
        "message" : "Data Produk Berhasil Dimasukkan"
    }
    return data

def show_all_produk():
    result = db.showallproduk()
    return result

def show_produk_by_nama_toko(param):
    result = db.showprodukbynamatoko(param)
    return result

def delete_produk_by_nama_produk(param):
    db.deleteprodukbynamaproduk(param)
    data = {
        "message" : "Data Berhasil Dihapus"
    }
    return data
def update_produk_by_id(param):
    db.updateproduk(param)
    data = {
        "message" : "Data Berhasil di Update"
    }
    return data